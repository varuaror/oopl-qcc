// -----------
// Classes.c++
// -----------

#include <cassert>  // assert
#include <iostream> // cout, endl
#include <utility>  // move

using namespace std;

template <typename T>
class A {
    private:
        T _v;

    public:
        A (const T& v) :
            _v (v)
            {}

        A (const A& rhs) :
            _v (rhs._v)
            {}

        A& operator = (const A& rhs) {
            _v = rhs._v;
            return *this;}

        ~A ()
            {}};

int main () {
    cout << "Classes.c++" << endl;

    A<int> x = 2;
    A<int> y = x;
    y = x;

    cout << "Done." << endl;
    return 0;}
