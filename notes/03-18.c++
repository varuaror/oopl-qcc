// -----------
// Mon, 18 Mar
// -----------

/*
N 650 C

Canvas
    ask and answer questions
    please be proactive
*/

/*
International Collegiate Programming Contest (ICPC)
40 years
part of the ACM, 80 years old, Turing Award

UT takes 4 teams of 3 students to a regional competition
at Baylor in Waco TX

110 regions in the world
11 regions in the US

65 other teams from 25 schools from 3 states

2014: our best team came in 2nd, Rice came in 1st, Morocco
2015: our best team came in 2nd, our 4 teams were in the top 7, Rice came in 1st, Thailand
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: 1st, Beijing
2018: 1st, Portugal
*/

std::cout instead of cout
std::endl instead of endl

#include <iostream> // cout, endl, ostream, <<

cout is an instance ostream that uses stdout

cout << 2;
cout << 3.5;
cout << "hi";
cout << flush; // to flush the buffer
cout << "\n";  // to output a new line
cout << endl;  // to do both

// overloaded the operator <<

int i = 2;
int j = 3;
int k = (i << j); // left bit shift

// operator overloading

// << does not modify the left or the right and returns a result

int k = (2 << 3);
cout << k;        // 16

// << takes an r-value for its two arguments, and returns an r-value

// r-value is a value that can be on the right of an assignment, but not on the left
// 2 is an r-value

int* p = &2; // no

i = 2;
2 = i; // no

// l-value can be on the left or the right
// i is an l-value

int* p = &i;

i = j;

i << j; // no, is expression, and expressions to be part of something bigger

int* p = &(i << j); // no

(i << j) = k; // no

i <<= j; // does not modify j, does modify i, this is a statement
2 <<= j; // no
i <<= 2;

int* p = &(i <<= j); // this not true in C or Java

i <<= j;
i = i << j;

// <<= takes an l-value on the left and an r-value on the right
// returns an l-value

k = (i <<= j); // in this example (i <<= j) is acting like an r-value

(i <<= j) <<= k; // in this example (i <<= j) is acting like an l-value
(i <<= j) = k;   // ok, but stupid
(i <<= j)++;

x = (i + j * k); // * goes first, precedence
x = (i - j - k); // i - j goes first, associativity, - is left-associative
x = (i = j = k); // j = k goes first, = is right-associative

=, *, -,  // are binary operators
!, *, ++, // are unary operators
? :       // ternary operator
()        // n-ary operator

int k = f(...) ? <then-value> : <else-value>;

int k = (n % 2) ? 58 : 36;

// ostream and overloads <<
// can ostream change the precedence    of <<? no
// can ostream change the associativity of <<? no
// can ostream change the arity         of <<? no
// can ostream the r-value/l-value nature of the args or the return of <<? yes

(cout << i) << j;
cout << flush;    // modifies cout

// osteam's << takes an l-value on the left, an r-value on the right, and returns an l-value

ostream cout(...); // in <iostream>, cout was defined to be an instance of ostream

ostream my_cout(???);

ostream* p = &cout;
         p = &(cout << i);

// can't define a new operator token, like ,,,
// can I define << on float?, no
// have define an operator using on OLD token and a NEW type

// Collatz Conjecture
// about 100 years old

/*
take a pos int
if even, divide by 2
otherwise, multiply by 3 and add 1
repeat until 1
*/

5 16 8 4 2 1

// the cycle length of  1 is 1
// the cycle length of  5 is 6
// the cycle length of 10 is 7

/*
assertions are good for preconditions, IF the input is NOT user input
are good for postconditions
are bad for testing
*/

/*
bad tests can hide bad code
*/

/*
1. run the code as is, confirm success
2. identify and fix the tests that are broken
3. run the code, confirm failure
4. fix the code
5. run the code, confirm success
*/

/*
password is 1234
*/

cout << "hi" << "bob";

int i = 2;
cout << i;

int i = 2;
int j = ++i;
cout << i;   // 3
cout << j;   // 3

int i = 2;
int j = i++;
cout << i;   // 3
cout << j;   // 2

f(++i);
g(i++);

++i;
i++;

for (int i = 0; i != s; i++)
    ...

int i = 0;
while (i != s) {
    ...
    i++;}

for (I i = 0; i != s; ++i)
    ...

I i = 0;
while (i != s) {
    ...
    ++i;}

++2; // no
2++; // no
++i; // returns an l-value
i++; // returns an r-value

++++i; // ok
i++++; // no

(i++)++; // no
++(i++); // no

++i++;   // ?
(++i)++;
++(++i);

int* p = &(++i);
int* p = &(i++); // no

int i = 2;
int j = 3;
int k = (i + j);
int k = (2 + 3);

int* p = &(i + j); // no

i += j;
i = i + j;

i += 3;
2 += j; // no

int* p = &(i += j);

int* p = &++i;
int* p = &i++; // no

// pretend that we don't have exceptions, like C

// make use of the return

int f (...) {
    ...
    if (<something is wrong>)
        return -1;
    ...

void g (...) {
    ...
    int i = f(...);
    if (i == -1)
        <something is wrong>

// use global

int h;

int f (...) {
    ...
    if (<something is wrong>)
        h = -1
        return ???
    ...

void g (...) {
    ...
    h = 0;
    int i = f(...);
    if (h == -1)
        <something is wrong>

// use an arg, but remember by reference

int f (..., int& h2) {
    ...
    if (<something is wrong>)
        h2 = -1
        return ???
    ...

void g (...) {
    ...
    int h = 0;
    int i = f(..., h);
    if (h == -1)
        <something is wrong>

void f1 (int** p) {
    ...}

void f2 (int*& r) {
    ...}

void f3 (int**& r) {
    ...}

int   i = 2;
int*  p = &i;
int** q = &p;

f1(i); // no
f1(p); // no
f1(q); // yes

f2(i); // no
f2(p); // yes
f2(q); // no

f3(i); // no
f3(p); // no
f3(q); // yes

struct Mammal {
    int};

struct Tiger : Mammal {
    int}

struct Lion : Mammal {
    int}

Lion    x(...);
Mammal  y = x;  // loses data that is specific to Lion
Mammal& r = x;  // no loss of data, still have to issue of accessing that data

// use exceptions

// catch by value

int f (...) {
    ...
    if (<something is wrong>) {
        Lion x(...);
        throw x;}               // throw always make a copy
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger e)          // catch by value makes a second copy
        <something is wrong>
    catch (Mammal e)         // catch by value results in a slice
        <something is wrong>
    ...

// catch by address

int f (...) {
    ...
    if (<something is wrong>) {
        Lion x(...);
        throw &x;}               // throw always make a copy
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger* e)          // catch by address is easily invalid
        <something is wrong>
    catch (Mammal* e)
        <something is wrong>
    ...

// catch by reference

int f (...) {
    ...
    if (<something is wrong>) {
        Lion x(...);
        throw x;}               // throw always makes a copy
    ...

void g (...) {
    ...
    try {
        ...
        int i = f(...);
        ...}
    catch (Tiger& e)          // catch by reference is ALWAYS what you want
        <something is wrong>
    catch (Mammal& e)
        <something is wrong>
    ...

char   s1[] = "abc"; // C   string
char   s2[] = "abc";
cout << (s1 == s2);  // false

String t1   = "abc"; // C++ string
String t2   = "abc";
cout << (t1 == t2);  // true

cout << (t1 == "abc"); // yes

/*
1. overloaded == with a C++ string and a C string, this too narrow of a solution
2. type conversion of C string to C++ string, this is what's happening
*/

void f (String s) {
    ...}

int main () {
    String t("abc");
    f(t);
    f("abc");  // yes
    return 0;}

try {
    <stuff>}
catch (Exception& e) { // catch anything
    <stuff>}

try {
    <stuff>}
catch (...) { // catch anything
    <stuff>}

// from a catch throwing a different type of object

try {
    ...}
catch (E& e) {
    ...
    if (...)
        throw H(...);
    ...}
catch (H& e) {
    ...}

// from a catch throwing a different object of the same type

try {
    ...}
catch (E& e) {
    ...
    if (...)
        throw E(...);
    ...}
catch (H& e) {
    ...}

// from a catch throwing the object that we caught

try {
    ...}
catch (E& e) {
    ...
    if (...)
        throw e;
    ...}
catch (H& e) {
    ...}

// from a catch throwing the object that we caught

try {
    ...}
catch (E& e) {
    ...
    if (...)
        throw;
    ...}
catch (H& e) {
    ...}

struct Tiger {...};

Tiger x(...); // making an instance (or an object) of type Tiger

int i; // making an instance (or an object) of type int

int i = 2;
int v = i;
++v;
cout << i; // 2
cout << v; // 3

/*
two tokens
    *
    &
two contexts
    next to the name of a variable
    next to the name of a type
*/

int  i = 2;
int* p = i;  // no
int* p = 2;  // no
int* p = 0;  // yes
int* p = &i; // l-value
int* p = &2; // no
++p;         // no, crazy
cout << i;   // 2
cout << p;   // the value of the pointer
cout << *p;  // 2
++*p;
cout << i;   // 3

int& r = &i; // no
int& r = i;  // r becomes indistinguishable from i

cout << (&r == &i); // true

cout << i; // 3
++r;
cout << i; // 4

r = 5;
cout << i; // 5

int j = 6;

r = j;
cout << i; // 6

int i = 2;
int j = ++i;
cout << i;          // 3
cout << j;          // 3
cout << (&j == &i); // false

int  i = 2;
int& r = ++i;
cout << i;          // 3
cout << r;          // 3
cout << (&r == &i); // true

int i = 2;
int j = f(i);
cout << i;          // 3
cout << j;          // 3
cout << (&j == &i); // false

int  i = 2;
int& r = f(i);
cout << i;          // 3
cout << r;          // 3
cout << (&r == &i); // true

int i = 2;
int j = i++;
cout << i;          // 3
cout << j;          // 2
cout << (&j == &i); // false

int  i = 2;
int& r = i++;       // no
cout << i;          // 3
cout << r;          // 3
cout << (&r == &i); // true
