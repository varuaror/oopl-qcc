// -----------
// Wed, 20 Mar
// -----------

/*
N 850 C

Canvas
    ask and answer questions
    please be proactive
*/

int a[2];
int a[2] = {};

A a[2];
A a[2] = {};
A a[2] = {2, 3};

/*
vector<T>
    front-loaded array

list<T>
    doubly-linked list

forward_list<T>
    singly-linked list
*/

int i;
cin >> i;

istringstream sin("2 3 4");
sin >> i;

cout << i;
ostringstream sout;
sout << i;

template <typename II, typename OI>
void copy (II b, II e, OI x) {
    while (b != e) {
        *x = *b;
        ++b;
        ++x;}}
/*
II -> input  iterator
OI -> output iterator
*/

template <typename FI, typename T>
void fill (FI b, FI e, T v) {
    while (b != e) {
        *b = v;
        ++b;}}

for (int i = 0; i != 10; ++i)
    s += i;

{
int i = 0;
while (i != 10) {
    s += i;
    ++i;}
}
cout << i; // no

for (int v : a)
    s += v;

{
auto b = begin(a);
auto e = end(a);
while (b != e) {
    int v = *b;
    s += v;
    ++b;}
}

int* a = new int[100];

int* p = new int(10);
...
delete p;

int* p = new int;
T*   q = new T;

/*
things you can do wrong with delete

1. incorrect use of []
2. didn't do delete
3. after delete, still using pointer
4. delete the same pointer more than once
5. delete the wrong address
*/

/*
garbage collector like in Java, Python
alternative is a memory checker, like Valgrind
*/

T  v(...);
T* a = new T[s];   // T(),  s times
fill(a, a + s, v); // =(T), s times
...
T* b = a;
delete [] a;

{
vector<T> x(s, v);
vector<T> y = x;
vector<T> z(10*s, v);
x = z;
}

template <typename T>
class my_vector {
    private:
        T*     _a;
        size_t _s;
    public:
        my_vector () {
            _a = nullptr;
            _s = 0;}
        my_vector (size_t s) {
            _a = new T[s];
            _s = s;
            fill(_a, _a + _s, T());}
        my_vector (size_t s, T v) {
            _a = new T[s];
            _s = s;
            fill(_a, _a + _s, v);}
        my_vector (initializer_list<T> il) {
            _a = new T[il.size()];
            _s = il.size();
            copy(_a, _a + _s, begin(il));}
        ~my_vector () {
            delete [] _a;}                 // well defined for nullptr
        size_t size () const {
            return _s;}
        const T& operator [] (int i) const {
            return (*const_cast<my_vector*>(this))[i];}      // the type of this is: const my_vector* const
        T& operator [] (int i) {
            return _a[i];}                                   // the type of this is:       my_vector* const
        void modify () { // an example of a non-const
            ...}
        const T* begin () const{
            return (*const_cast<my_vector*>(this)).begin();} // the type of this is: const my_vector* const
            return const_cast<my_vector*>(this)->begin();}   // the type of this is: const my_vector* const
        T* begin () {
            return _a;}
        const T* end () const {
            return const_cast<my_vector*>(this)->end();}     // the type of this is: const my_vector* const
        T* end () {
            return _a + _s;}

int main () {
    my_vector<int> x;
    ...fill...
    cout << x.size();
    x.modify();
    ? b = begin(x);
    ? b = x.begin();
    cout << x[2];            // ?
    cout << x.operator[](2); // ?
    x[2] = 3;

    const my_vector<int> cx;
    ...fill...
    cout << cx.size();
    cx.modify();              // no
    cout << cx[2];            // ?
    cout << cx.operator[](2); // ?
    cx[2] = 3;

    my_vector<int> y = x;
    my_vector<int> z = {2, 3, 4};

int i = 2;
int j = 3;

cout << int(); // 0

my_vector<int>  x(10, 2);
my_vector<int>* p = &x;

cout << x.size();

cout << *p.size();   // no
cout << (*p).size();
cout << p->size();

int  i = 2;
int* p = &i;
cout << i;
cout << *p;

class A {};

int main () {
    A x;       // default constr
    A y = x;   // copy    constr
    y = x;     // copy    assignment
    return 0;} // destructor

template <typename T>
class A {
    private:
        T _v;

    public:
        A () {}          // T() will automatically run, default constr

        A () :
            _v ()        // T(), default constr
            {}

        A () = default;  // C++11

        A (T v) {        // most expensive, double copy, T(), operator=(T)
            _v = v;}

        A (const T& v) { // cheaper way of saying (T v); T() will automatically run, default constr
            _v = v;}     // operator=(T)

        A (const T& v) : // cheaper still
            _v (v)       // member initialization list; T(T), copy constr
            {}

        A (const A& other) { // T() will automatically run, default constr
            _v = other._v;}  // operator=(T)

        A (const A& other) : // cheaper
            _v (other._v)    // member initialization list; T(T), copy constr
            {}

        A (const A&) = default;

        A& operator= (const A& other) {
            _v = other._v;               // operator=(T)
            return *this;}

        A& operator= (const A&) = default;

        ~A () {}

        ~A () = default;

void f (A t) { // copy constr
    ...}

int main () {
    A<int> x;
    A<int> y = 2;
    A<int> z = y; // copy constr
    f(x);
    x = y;
    x.operator=(y);
    x = (y = z);
    x = y.operator=(z);
    (x = y) = z;
    return 0;}

int i; // define
i = 2; // assign

int i = 2; // initialize

struct A {
    int _iv;}; // one per instance of A

/*
non-static data members
instance members
*/

struct B {
    static int _cv;}; // one for all instances of B to share

int main () {
    B x;
    B y;
    return 0;}

/*
static data members
class members
*/

template <typename T>
struct C {
    static int _cv;}; // one for all instance of C<T> to share

struct D {};

int main () {
    C<int>    x;
    C<double> y;
    return 0;}

/*
static data members
class members
*/

assert(&A<int>::cv == &A<int>::cv);

assert(&A<int>::cv != &A<double>::cv);

A<int> x;
A<int> y;
assert(&x.cv == &y.cv);

A<int>    x;
A<double> y;
assert(&x.cv != &y.cv);
