.DEFAULT_GOAL := all

all:

clean:
	cd examples/c++; make clean

config:
	git config -l

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/oopl-qcc.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add examples
	git add makefile
	git add notes
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	@rsync -r -t -u -v --delete            \
    --include "Hello.c++"                  \
    --include "Assertions.c++"             \
    --include "UnitTests1.c++"             \
    --include "UnitTests2.c++"             \
    --include "UnitTests3.c++"             \
    --include "Coverage1.c++"              \
    --include "Coverage2.c++"              \
    --include "Coverage3.c++"              \
    --include "IsPrime.c++"                \
    --include "IsPrimeT.c++"               \
    --include "Exceptions.c++"             \
    --include "Variables.c++"              \
    --include "Arguments.c++"              \
    --include "Operators.c++"              \
    --include "Incr.c++"                   \
    --include "IncrT.c++"                  \
    --include "Consts.c++"                 \
    --include "Arrays1.c++"                \
    --include "Equal.c++"                  \
    --include "EqualT.c++"                 \
    --include "Copy.c++"                   \
    --include "Iterators.c++"              \
    --include "Fill.c++"                   \
    --include "Factorial.c++"              \
    --include "RangeIterator.c++"          \
    --include "Functions.c++"              \
    --include "Iteration.c++"              \
    --include "Range.c++"                  \
    --include "Types.c++"                  \
    --include "Arrays2.c++"                \
    --include "Vector1.c++"                \
    --include "Vector1T.c++"               \
    --include "Vector2.c++"                \
    --include "Vector2T.c++"               \
    --include "FunctionOverloading.c++"    \
    --include "Move.c++"                   \
    --include "Vector3.c++"                \
    --include "Vector3T.c++"               \
    --include "Vector4.c++"                \
    --include "Vector4T.c++"               \
    --include "Classes.c++"                \
    --include "InstanceVariables.c++"      \
    --include "ClassVariables.c++"         \
    --include "InstanceMethods.c++"        \
    --include "ConstMethods.c++"           \
    --include "ClassMethods.c++"           \
    --include "Shapes1.c++"                \
    --include "Shapes1T.c++"               \
    --include "MethodOverriding1.c++"      \
    --include "Shapes2.c++"                \
    --include "Shapes2T.c++"               \
    --include "MethodOverriding2.c++"      \
    --include "Shapes3.c++"                \
    --include "Shapes3T.c++"               \
    --exclude "*"                          \
    ../../examples/c++/ examples/c++/
	@rsync -r -t -u -v --delete            \
    --include "Stack1.uml"                 \
    --include "Stack1.svg"                 \
    --include "Stack2.uml"                 \
    --include "Stack2.svg"                 \
    --include "Shapes1.uml"                \
    --include "Shapes1.svg"                \
    --include "Shapes2.uml"                \
    --include "Shapes2.svg"                \
    --include "Shapes3.uml"                \
    --include "Shapes3.svg"                \
    --exclude "*"                          \
    ../../examples/uml/ examples/uml/
